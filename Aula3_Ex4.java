import java.util.Scanner;

class Calc {
// Declarações
private double val1, val2, val3, output;
private String arg;
// Construtores
public Calc(String vals, String arg) {
val1 = Double.parseDouble(vals.split(" ")[0]);
val2 = Double.parseDouble(vals.split(" ")[1]);
val3 = Double.parseDouble(vals.split(" ")[2]);
this.arg = arg.toUpperCase();
}
// Métodos
public double calcular() {
if (arg.equals("A")) {
output = (val1 + val2 + val3) / 3.0;
}
else if (arg.equals("P")) {
output = ( (5 * val1) + (3 * val2) + (2 * val3) ) / 10.0;
}

return output;

}

}

public class Aula3_Ex4 {
// Declarações
private static String vals, arg;
// Instanciações
private static Scanner teclado = new Scanner(System.in);
// Métodos

    public static void main(String[] args) {
        System.out.printf("Notas $ ");
        vals = teclado.nextLine();
        System.out.printf("Operação $ ");
        arg = teclado.nextLine();

            Calc calc = new Calc(vals, arg);
            System.out.println(calc.calcular());

    // Fechamento de Objetos
    teclado.close();
    }

}
