import java.util.Scanner;

public class Aula1_Ex1 {

// Criação do objeto do input
private static Scanner entrada = new Scanner(System.in); // Classe nome_objeto = new Classe(Parâmetros_Construtor)
// static => pertence a própria classe e não a uma instância 
// private => somente acessível na própria classe

    public static void main(String[] args) {
    // Declarações
    double a, b, c;

        System.out.print("Digite o valor de A $ ");
        a = entrada.nextDouble(); // Chamada do método no novo objeto criado a partir do Scanner()
        System.out.print("Digite o valor de B $ ");
        b = entrada.nextDouble();
        System.out.print("Digite o valor de C $ ");
        c = entrada.nextDouble();

            System.out.printf("Triângulo: %.3f\n", (a * c) / 2);
            System.out.printf("Círculo: %.3f\n", Math.pow(c, 2) * Math.PI);
            System.out.printf("Trapézio: %.3f\n", ((a + b) * c) / 2);
            System.out.printf("Quadrado: %.3f\n", Math.pow(b, 2));
            System.out.printf("Retângulo: %.3f\n", a * b);

                entrada.close();

    }

}
