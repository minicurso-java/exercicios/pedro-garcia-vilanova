// Bibliotecas
import java.util.Scanner;

public class Aula2_Ex8 {
// Declarações
private static float[] vals_xy = new float[2];
private static String[] teclado_split;

// Instanciações 
private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
       teclado_split = teclado.nextLine().split(" ");

// Conversão de String[] para float[]
for (int ctr = 0; ctr <= 1; ctr++) {
    vals_xy[ctr] = Float.parseFloat(teclado_split[ctr]);
}

        // Possibilidades dos Quadrantes
        if (vals_xy[0] > 0) { // Pode ser o Q1 ou Q4
            
            if (vals_xy[1] > 0) // Só pode ser o Q1
                System.out.println("Q1");
            else if (vals_xy[1] < 0) // Só pode ser o Q4
                System.out.println("Q4");

        }
        
        else if (vals_xy[0] < 0) { // Pode ser o Q2 ou Q3

            if (vals_xy[1] > 0) // Só pode ser o Q2
                System.out.println("Q2");
            else if (vals_xy[1] < 0) // Só pode ser o Q3
                System.out.println("Q3");

        }

            // Possibilidades dos Eixos e Origem
            if (vals_xy[0] == 0 && vals_xy[1] == 0) // So pode ser a origem
                    System.out.println("Origem");
                else if (vals_xy[1] == 0)
                    System.out.println("Eixo X"); // Só pode ser no Eixo X
                else if (vals_xy[0] == 0) 
                    System.out.println("Eixo Y"); // Só pode ser no Eixo Y

    // Fechar objetos
    teclado.close();
    
    }

}
