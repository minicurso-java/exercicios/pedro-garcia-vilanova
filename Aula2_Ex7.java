// Bibliotecas
import java.util.Scanner;

public class Aula2_Ex7 {
// Declarações
private static String[] vals_string; // Será inicializado null
private static float[] vals_float = new float[3]; // Necessário para modificar valores do array &&
/*
 * Uso do new:
 * Basicamente para alocar memória
 * Ex:
 * 
 * Iniciar arrays
 * float[] algo = new float[n]
 * 
 * Instanciar algo
 * Classe nome_objeto = new Classe(Argumentos_Construtor)
 */


// Criação de objetos
private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("X Y Cód.Operação"); 
        System.out.println("(1)Soma (2)Subtração (3)Divisão (4)Multiplicação");

        vals_string = teclado.nextLine().split(" "); // Isso aqui já inicializa(*) automaticamente ??
          //(*)

// Conversão de String para float
for (int ctr = 0; ctr <= 1; ctr++) { // = 0 para inicializar a variável
    vals_float[ctr] = Float.parseFloat(vals_string[ctr]);
}

        switch (vals_string[2]) {
            case "1":
                System.out.println(vals_float[0] + vals_float[1]);
            break;
            case "2":
                System.out.println(vals_float[0] - vals_float[1]);
            break;
            case "3":
                System.out.println(vals_float[0] / vals_float[1]);
            break;
            case "4":
                System.out.println(vals_float[0] * vals_float[1]);
            break;
        }
    
    // Fechamento de objetos
    teclado.close();
    }
} 

/*
 * ??: Não tenho certeza
 * &&: Não sei o porquê
 */