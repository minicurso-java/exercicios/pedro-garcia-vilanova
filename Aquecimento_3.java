import java.util.Scanner;

public class Aquecimento_3 {
// Declarações
private static float nuns, soma;
private static int ctr;
// Instanciação    
private static Scanner teclado = new Scanner(System.in);
// Métodos
private static float med(float som) {
    return som / ctr;
}
public static void main(String[] args) {
    System.out.println("Encerrar(-1)");

    do {
        nuns = teclado.nextFloat();
        ctr++;

            if (nuns == -1f) {
                System.out.println("Incluir -1 na média ou encerrar? ('I'ncluir)('E'ncerrar) $ ");
                if (teclado.next().charAt(0) == 'E')
                    break;
            }
        
        soma += nuns;

    } while(true);
    ctr--;

        System.out.println("Média = " + med(soma));

// Fechamento dos objetos
teclado.close();

}

}
