import java.util.Scanner;

public class Aula6_At2 {
private static int[] vetor = {1, 2, 3, 4, 5, 6, 7, 8};
private static int[] indices = new int[2]; 
private static int temp;
private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        
        System.out.printf("O vetor salvo é ");
        for (int ctr = 0; ctr < vetor.length; ctr++) {
            System.out.printf("%d", vetor[ctr]);
        }

            System.out.println();
            System.out.printf("$ ");
            indices[0] = teclado.nextInt();
            System.out.printf("$ ");
            indices[1] = teclado.nextInt();

                // teclado[0] = 0 teclado[1] = 1
                // [1, 2]
                // vetor[0] = 1
                // vetor[1] = 2
                temp = vetor[indices[0]]; // temp = 1
                vetor[indices[0]] = vetor[indices[1]]; // vetor[0] = 2 || vetor[0] = vetor[1]
                vetor[indices[1]] = temp; 
                // vetor[0] = 2
                // vetor[1] = 1

                    System.out.printf("O novo vetor é ");
                    for (int ctr = 0; ctr < vetor.length; ctr++) {
                        System.out.printf("%d", vetor[ctr]);
                    }
                    System.out.println();            
    }
}
