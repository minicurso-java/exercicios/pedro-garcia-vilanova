import java.util.Scanner;

class Maquina_Fah {
  // Atributos
  private float celsius;

  public Maquina_Fah(float celsius) {
    this.celsius = celsius;
  }

  // Métodos
  public float calc() {
    return ((celsius * 9) / 5) + 32;
  }
}

public class Aula5_Ex5 {
  // Scanner
  private static Scanner teclado = new Scanner(System.in);

  public static void main(String[] args) {
    // Métodos
    do {
      Maquina_Fah fah = new Maquina_Fah(teclado.nextFloat());
      System.out.println(fah.calc());
      teclado.nextLine(); // Arrumar o nulo pendente

      System.out.printf("Deseja retornar?(s/n) $ ");
    } while (teclado.nextLine().charAt(0) == 's');

  // Fechamento de objetos
  teclado.close();
  }
}