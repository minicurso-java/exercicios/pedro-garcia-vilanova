import java.util.Scanner;

public class Aula5_Ex2 {
    public static void main(String[] args) {
        // Teclado
        Scanner teclado = new Scanner(System.in);
        int n_combs;
        int gasolina = 0, alcool = 0, diesel = 0;

        do {
            n_combs = teclado.nextInt();

                switch (n_combs) { // case rule
                    case 1 -> alcool++;
                    case 2 -> gasolina++;
                    case 3 -> diesel++;
                }

        } while (n_combs != 4);

        System.out.println("MUITO OBRIGADO");
        System.out.println("Álcool: "+alcool);
        System.out.println("Gasolina: "+gasolina);
        System.out.println("Diesel: "+diesel);

        //.close()
        teclado.close();
    }
}
