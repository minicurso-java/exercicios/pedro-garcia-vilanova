import java.util.Scanner;

public class Aula6_At1 {
// Criação do Array
private static double[] array = new double[7];
// Instanciação
private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        for (int ctr = 0; ctr < 7; ctr++) {
            System.out.printf("$ ");
            array[ctr] = teclado.nextDouble();
        }
        
            System.out.println("O valor do primeiro elemento: " + array[0]);
            System.out.println("O valor do último elemento: " + array[6]);

    // Fechamento de objetos
    teclado.close();
    
    }

}