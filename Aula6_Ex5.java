import java.util.*;

public class Aula6_Ex5 {

private static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

	// Criação de vetores
	System.out.printf("Quantas pessoas serão? $ ");
	String[] nome = new String[teclado.nextInt()];
	int[] idade = new int[nome.length];
	teclado.nextLine();

		// Captura do teclado
		for (int i = 0; i < idade.length; i++) {
			System.out.printf("Nome $ ");
			nome[i]	= teclado.nextLine();

			System.out.printf("Idade $ ");
			idade[i] = teclado.nextInt();
			teclado.nextLine();	
		}

		// Verificação da maior idade
		int maior = idade[0];
		int vet = 0;
		for (int i = 1; i < idade.length; i++) {
			if (idade[i] > maior) { // Se a idade[0] já for o maior, vet = 0 
				maior = idade[i];
				vet = i;
			}
		}

			System.out.println("A pessoa mais velha é " + nome[vet]);

	// close()
	teclado.close();
	}
}

/*
 * Feito no VIm :)
 */
