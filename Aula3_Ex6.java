import java.util.Scanner;

public class Aula3_Ex6 {
// Declarações 
// Instanciação
private static Scanner teclado = new Scanner(System.in);
// Métodos
private static void retVogais(String[] input) {

    String[] vogais = {"a", "e", "o", "u", "i"}; // Dicionário de vogais
    int vtr_input = 0;
    boolean isVogal = false;


    while (vtr_input < input.length) {
    for (int i = 0; i < 5; i++) { // Caso ele achar uma vogal, será true
        if (input[vtr_input].toUpperCase().equals(vogais[i].toUpperCase())) {
            isVogal = true;
        }
    }

        if (isVogal == false) { // Se ele varrer tudo e não rodar o if (), será = false
            System.out.printf("%s", input[vtr_input]);
        }

    // Reset das variáveis 
    isVogal = false;
    vtr_input++;

}}

    public static void main(String[] args) {
        System.out.printf("$ ");
            retVogais(teclado.nextLine().split(""));

    teclado.close();
    }}
