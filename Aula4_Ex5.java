import java.util.*;

public class Aula4_Ex5 {
// Instanciações
private static Scanner teclado = new Scanner(System.in);

// Declarações
private static int num;
// Métodos

public static void main(String[] args) {

    System.out.printf("$ ");
    num = teclado.nextInt();

        for (int ctr = 1; ctr <= num; ctr++) {
            System.out.println(ctr + " " + Math.pow(ctr, 2) + " " + Math.pow(ctr, 3));
        }

teclado.close();
}}
