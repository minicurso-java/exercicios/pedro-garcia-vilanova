import java.util.Scanner;

public class Aquecimento_2 {
// Declarações
static private int n1;
// Instanciações
static private Scanner teclado = new Scanner(System.in);
// Métodos
private static boolean mul_2(int num1) {
    if (num1 % 2 == 0)
        return true;
    else
        return false;
}

    public static void main(String[] args) {
        System.out.printf("$ ");
        n1 = teclado.nextInt();

            if (mul_2(n1) == true) 
                System.out.println("O número " + n1 + " é múltiplo de 2");
            else    
                System.out.println("Não é múltiplo");

    // Fechamento dos objetos
    teclado.close();

    }

}
