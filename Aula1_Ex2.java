// Bibliotecas
import java.util.Scanner;


class Produtorio {
// Declarações
private double a, b, c, d; // Bem 'aqui(*)'

// Construtores
public Produtorio(double a, double b, double c, double d) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    // Redireciona para a declaração 'aqui(*)'
}

// Métodos
public double dif_produto() {
    return a * b - c * d;
}


}


public class Aula1_Ex2 {
// Declarações
private static double a, b, c, d;

// Criação dos objetos
private static Scanner teclado = new Scanner(System.in); 

    public static void main(String[] args) {
        System.out.printf("Digite A $ ");
        a = teclado.nextDouble();
        System.out.printf("Digite B $ ");
        b = teclado.nextDouble();
        System.out.printf("Digite C $ ");
        c = teclado.nextDouble();
        System.out.printf("Digite D $ ");
        d = teclado.nextDouble();
        
            Produtorio produtorio = new Produtorio(a, b, c, d);
            System.out.println("Diferença = "+ produtorio.dif_produto()); 

    // Fechar objetos
    teclado.close();
 // produtorio.close();
    
    }

}

/*
 * Eu sei que o Java conta com a interface 'implements AutoCloseable', mas não sei implementar ainda :(
 */