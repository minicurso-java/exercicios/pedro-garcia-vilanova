import java.util.Scanner;

public class Exercício_Cabuloso {
// Declarações
private static int n, quebras;
private static String[] lc; //= new String[3]; ??
// Instaciações
private static Scanner teclado = new Scanner(System.in);
// Métodos

    public static void main(String[] args) {
        System.out.printf("$ ");
        n = teclado.nextInt();

        // Consumir a linha pendente depois do nextInt()
        teclado.nextLine();

        for (int ctr = 1; ctr <= n; ctr++) {
            System.out.printf("$ ");
            lc = teclado.nextLine().split(" ");

            if (Integer.parseInt(lc[0]) > Integer.parseInt(lc[1])) {
                quebras += Integer.parseInt(lc[1]); 
            }
        }

                System.out.printf("\n%d\n\n", quebras);

    // Fechamento de objetos
    teclado.close();

    }

}

/*
 * ??: Não entendo porque tem hora que é necessário iniciar a variável.
 * A JVM não já cuida disto para mim?
 */