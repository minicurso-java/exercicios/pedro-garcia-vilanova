import java.util.Scanner;

public class Aquecimento_1 {
// Declarações
private static int n1, n2;
// Instancia
private static Scanner teclado = new Scanner(System.in);
// Métodos
private static int mult(int num1, int num2) {
    return num1 * num2;
}

    public static void main(String[] args) {
        System.out.printf("$ ");
        n1 = teclado.nextInt();

        System.out.printf("$ ");
        n2 = teclado.nextInt();

            System.out.println("O resultado é " + mult(n1, n2));
    
    // Fechamento dos objetos
    teclado.close();

    }

}
