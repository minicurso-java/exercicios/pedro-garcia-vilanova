import java.util.ArrayList; // Biblioteca de lista

import java.util.Scanner;

class n_primos {
// Declarações
private int n;
ArrayList<Integer> output_lista = new ArrayList<>();
Integer[] output;
// Construtores
public n_primos(int n) {
this.n = n;
}

public Integer[] get_nprimos() { // Garante segurança
    return output;
}

public void n_primos_calc() {

    int primos = 2; // Começa a partir do "2"
    int divisores = 0; // Para ser primo divisores == 2
    int nuns = 1; // Divisor dos primos

    for (int ctr = 0; ctr < this.n; ctr++) {
        do {
            if (primos % nuns == 0) { // Teste de divisor
                divisores++;
            }
        nuns++; // Testa outro divisor
        } while (nuns <= primos); // Fica em loop até o divisor ser maior que o dividendo

            if (divisores == 2) { // Verificação de primo
                output_lista.add(primos); // Salvar o primo no output_lista
            }
            else { // Caso ele não ache um primo ele vai repetir o for ()
                ctr--;
            }

        // Reset das variáveis
        divisores = 0; 
        primos++; // Calcula o próximo número
        nuns = 1;

    }
// Lista -> Array
Integer[] output_vetor = output_lista.toArray(new Integer[0]);
output = output_vetor;
}
}

public class Aula6_At3 {
// Declarações
static public int n;
public static Integer[] output;
// Instaciações
private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.printf("$ ");
        n = teclado.nextInt();

        n_primos primos = new n_primos(n);
        primos.n_primos_calc();
        output = primos.get_nprimos();

        for (int ctr = 0; ctr < output.length; ctr++) {
            System.out.println(output[ctr]);
        }

    // Fechamento do objeto
    teclado.close();

    }

}
