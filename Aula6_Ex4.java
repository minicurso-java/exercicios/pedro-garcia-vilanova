import java.util.*; // Vou usar o Scanner e ArrayList

public class Aula6_Ex4 {
  // Atributos
  private static Scanner teclado = new Scanner(System.in); // Scanner
  private static ArrayList<Double> nuns = new ArrayList<Double>(); // Cria "vetores" dinâmicos (Listas)
  // Uso da classe Wrapper para armazenamento

  // Métodos
  public static void main(String[] args) {

    double input; // Armazena os elementos para adicionar na lista
    do {
      System.out.printf("(0 para terminar)$ ");
      input = teclado.nextDouble();
      
      nuns.add(input); // .add adiciona na lista
    } while (input != 0);

      int vet = 0; // Registrador do vetor do maior
      double maior = nuns.get(0);
      for (int i = 1; i < nuns.size(); i++) { 
        if (nuns.get(i) > maior) {
          maior = nuns.get(i);
          vet = i;
        }
      }

          System.out.println("O maior número foi " + maior + " na posição " + vet);

    // Fechamento de objetos
    teclado.close();

  }
}